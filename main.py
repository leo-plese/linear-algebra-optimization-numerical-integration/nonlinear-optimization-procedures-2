from math import sqrt, log
from matrix import Matrix
from random import uniform

EPS = 1e-6  # EPS for number comparison (==, <, >)
DIV_NUM_ITER = 100
DIV_MAX_NUM = 1e50
DIV_MIN_NUM = 1e-50


class DivergenceException(Exception):
    def __init__(self, msg, x, f_x, num_iter, f_called_times):
        self.msg = msg
        self.x = x
        self.f_x = f_x
        self.num_iter = num_iter
        self.f_called_times = f_called_times


class GoalFunction():

    # is_multi_input - False by def; for simplex: True by def (False for iterative funs only)
    def __init__(self, f=None, is_multi_input=False):
        self.f = f
        self.called_times = 0
        self.is_multi_input = is_multi_input

    def evaluate(self, x, count_eval=True):
        if count_eval:
            self.called_times += 1

        if self.is_multi_input:
            return self.f(*x)
        else:
            return self.f(x)


class SurrogateGoalFunction():
    def __init__(self, f=None):
        self.f = f
        self.called_times = 0

    def evaluate(self, x, count_eval=True):
        if count_eval:
            self.called_times += 1

        return self.f(x)


def get_unimodal_interval(F, h, x0):
    m = x = x0
    l = m - h
    r = m + h

    step = 1

    fl = F.evaluate(l)
    fm = F.evaluate(m)
    fr = F.evaluate(r)

    if fm < fr and fm < fl:
        return l, r
    elif fm > fr:
        while fm > fr:
            l = m
            m = r
            fm = fr
            r = x + h * 2 ** step
            step += 1
            fr = F.evaluate(r)
        return l, r
    else:
        while fm > fl:
            r = m
            m = l
            fm = fl
            l = x - h * 2 ** step
            step += 1
            fl = F.evaluate(l)
        return l, r


def golden_ratio_with_interval(F, a, b, e=1e-6):
    k = (sqrt(5) - 1) / 2

    c = b - k * (b - a)
    d = a + k * (b - a)
    fc = F.evaluate(c)
    fd = F.evaluate(d)

    i = 1
    while ((b - a) > e):

        if fc < fd:
            b = d
            d = c
            c = b - k * (b - a)
            fd = fc
            if ((b - a) > e):
                fc = F.evaluate(c)
        else:
            a = c
            c = d
            d = a + k * (b - a)
            fc = fd
            if ((b - a) > e):
                fd = F.evaluate(d)
        i += 1

    return F.called_times, a, b


def golden_ratio_with_point(F, h, x0, e=1e-6):
    a, b = get_unimodal_interval(F, h, x0)

    return golden_ratio_with_interval(F, a, b, e)


def get_vect_norm(vect):
    return sqrt(sum([x ** 2 for x in vect]))




def grad_descent_method(f_goal, d_f_goal, x0, h, golden_ration_yn, e):
    d_f_x = d_f_goal(*x0)

    x = x0
    n = len(x)

    F = GoalFunction(f_goal)

    grad_norm = get_vect_norm(d_f_x)
    num_iter = 0

    best_f_val = None
    best_x_val = None
    best_f_val_bef_iter = 0
    while grad_norm >= e:
        num_iter += 1

        if best_f_val_bef_iter == DIV_NUM_ITER:
            raise DivergenceException("Divergence warning!", best_x_val, best_f_val, num_iter, F.called_times)


        f_eval_x = f_goal(*x)
        F.called_times += 1
        if best_f_val is None:
            best_f_val = f_eval_x
            best_x_val = x
            best_f_val_bef_iter += 1
        else:
            if abs(f_eval_x) >= DIV_MAX_NUM or abs(f_eval_x) <= DIV_MIN_NUM:
                raise DivergenceException("Divergence warning - too big/small x!", best_x_val, best_f_val, num_iter, F.called_times)
            if f_eval_x < best_f_val:  # better result
                best_f_val = f_eval_x
                best_x_val = x
                best_f_val_bef_iter = 0
            else:  # = ili >
                best_f_val_bef_iter += 1

        if golden_ration_yn:
            Flambdas = [(lambda param_lambda, ind=j: x[ind] - param_lambda * d_f_x[ind]) for j in range(n)]
            f_goal_lamm = lambda lamm: f_goal(*tuple([Flambdas[j](lamm) for j in range(n)]))
            F.f = f_goal_lamm
            _, a_lamm, b_lamm = golden_ratio_with_point(F, h=h, x0=0, e=e)

            a_x = [x[j] - a_lamm * d_f_x[j] for j in range(n)]
            b_x = [x[j] - b_lamm * d_f_x[j] for j in range(n)]
            x = [(a_x[j] + b_x[j]) / 2 for j in range(n)]
        else:
            x = [x[i] - d_f_x[i] for i in range(n)]

        d_f_x = d_f_goal(*x)
        grad_norm = get_vect_norm(d_f_x)

    return num_iter, F.called_times, x


def solve_equations_system(A, b):
    try:
        y = A.substitute_forward(b)
        x = A.substitute_backward(y)

        return x
    except ValueError as err:
        print(str(err))


def decompose_lup(A, b):
    try:
        p_vect, n_permutations = A.decompose_lup()

        b = b.reorder_elements(p_vect)

        return b, n_permutations
    except ValueError as err:
        print(str(err))


def get_delta_x(H, d_f_x):
    d_f_x_new, _ = decompose_lup(H, d_f_x)
    return solve_equations_system(H, d_f_x_new)


def newton_raphson_method(f_goal, d_f_goal, H_f_goal, x0, h, golden_ration_yn, e):
    d_f_x = d_f_goal(*x0)
    H_f_x = H_f_goal(*x0)

    x = x0
    n = len(x)

    F = GoalFunction(f_goal)

    grad_norm = get_vect_norm(d_f_x)
    num_iter = 0

    best_f_val = None
    best_x_val = None
    best_f_val_bef_iter = 0
    while grad_norm >= e:
        num_iter += 1

        if best_f_val_bef_iter == DIV_NUM_ITER:
            raise DivergenceException("Divergence warning!", best_x_val, best_f_val, num_iter, F.called_times)

        f_eval_x = f_goal(*x)
        F.called_times += 1
        if best_f_val is None:
            best_f_val = f_eval_x
            best_x_val = x
            best_f_val_bef_iter += 1
        else:
            if abs(f_eval_x) >= DIV_MAX_NUM or abs(f_eval_x) <= DIV_MIN_NUM:
                raise DivergenceException("Divergence warning - too big/small x!", best_x_val, best_f_val, num_iter,
                                          F.called_times)
            if f_eval_x < best_f_val:  # better result
                best_f_val = f_eval_x
                best_x_val = x
                best_f_val_bef_iter = 0
            else:  # = ili >
                best_f_val_bef_iter += 1


        H_mat = Matrix(n, n, H_f_x)
        d_f_x_mat = Matrix(n, 1, [[d_f_el] for d_f_el in d_f_x])
        delta_x = get_delta_x(H_mat, d_f_x_mat).elements
        delta_x = [delta_xx[0] for delta_xx in delta_x]

        if golden_ration_yn:
            Flambdas = [(lambda param_lambda, ind=j: x[ind] - param_lambda * delta_x[ind]) for j in range(n)]
            f_goal_lamm = lambda lamm: f_goal(*tuple([Flambdas[j](lamm) for j in range(n)]))
            F.f = f_goal_lamm
            _, a_lamm, b_lamm = golden_ratio_with_point(F, h=h, x0=0, e=e)

            a_x = [x[j] - a_lamm * delta_x[j] for j in range(n)]
            b_x = [x[j] - b_lamm * delta_x[j] for j in range(n)]
            x = [(a_x[j] + b_x[j]) / 2 for j in range(n)]
        else:
            x = [x[i] - delta_x[i] for i in range(n)]

        d_f_x = d_f_goal(*x)
        H_f_x = H_f_goal(*x)
        grad_norm = get_vect_norm(d_f_x)

    return num_iter, F.called_times, x


def hooke_jeeves_stopping_criterion_not_ok(dx, e):
    n = len(e)
    for i in range(n):
        if dx[i] > e[i]:
            return True
    return False


def hooke_jeeves_explore(F, xp, dx):
    x = xp[:]
    n = len(x)
    for i in range(n):
        p = F.evaluate(x)
        x[i] += dx[i]
        n = F.evaluate(x)
        if n >= p:
            x[i] -= 2 * dx[i]
            n = F.evaluate(x)
            if n >= p:
                x[i] += dx[i]
    return x


def hooke_jeeves_method(f_goal, dx, x0, e, is_multi_input=True, is_surrogate_goal_fun=False):
    e_default = 1e-6
    dx_default = 0.5

    if e is None:
        e = [e_default for _ in range(len(x0))]
    if dx is None:
        dx = [dx_default for _ in range(len(x0))]

    if not is_surrogate_goal_fun:
        F = GoalFunction(f_goal, is_multi_input=is_multi_input)
    else:
        F = f_goal

    n = len(x0)

    xb = x0[:]
    xp = x0[:]

    while hooke_jeeves_stopping_criterion_not_ok(dx, e):
        xn = hooke_jeeves_explore(F, xp, dx)

        if F.evaluate(xn) < F.evaluate(xb):
            xp = [(2 * xn[i] - xb[i]) for i in range(n)]
            xb = xn[:]
        else:
            dx = [0.5 * dx[i] for i in range(n)]
            xp = xb[:]

    return F.called_times, xb


def get_ok_point_for_explicit_constraints(x, xd, xg):
    for i in range(len(x)):
        if x[i] < xd[i]:
            x[i] = xd[i]
        elif x[i] > xg[i]:
            x[i] = xg[i]

    return x[:]


def inequality_constraints_ok(G, x):
    return G(*x) == 0


def shift_point_towards_centroid(p, xc):
    n = len(xc)
    p = [0.5 * (p[i] + xc[i]) for i in range(n)]
    return p[:]


def get_centroid(x):
    n = len(x)

    dim = len(x[0])
    xc = [sum([x[i][d] for i in range(n)]) / n for d in range(dim)]

    return xc[:]


def generate_2n_points(x0, xd, xg, G):
    xc = x0[:]

    x = []
    n = len(xd)

    for t in range(2 * n):
        p_new = []
        for i in range(n):
            p_new.append(uniform(xd[i], xg[i]))
        while not inequality_constraints_ok(G, p_new):
            p_new = shift_point_towards_centroid(p_new, xc)
        x.append(p_new)
        xc = get_centroid(x)

    return x


def get_index_h(x_eval):
    ind = 0
    max_val = x_eval[0]
    for i in range(1, len(x_eval)):
        if x_eval[i] > max_val:
            max_val = x_eval[i]
            ind = i
    return ind


def get_index_h2(x_eval, h_ind):
    x_eval_wo_h = [x_eval[i] if i != h_ind else float('-inf') for i in range(len(x_eval))]

    return get_index_h(x_eval_wo_h)


def get_index_l(x_eval):
    ind = 0
    min_val = x_eval[0]
    for i in range(1, len(x_eval)):
        if x_eval[i] < min_val:
            min_val = x_eval[i]
            ind = i
    return ind


def get_centroid_wo_h(x, h_ind):
    n = len(x)
    x_without_h = [x[i] for i in range(n) if i != h_ind]
    n -= 1

    dim = len(x[0])
    xc = [sum([x_without_h[i][d] for i in range(n)]) / n for d in range(dim)]

    return xc[:]


def get_reflection_point(xh, xc, alfa):
    d = len(xc)
    xr = [(1 + alfa) * xc[i] - alfa * xh[i] for i in range(d)]

    return xr[:]


def simplex_stopping_criterion_ok(f_xc, x, e, F):
    n = len(x)
    dev = sqrt(sum([(F.evaluate(xi) - f_xc) ** 2 for xi in x]) / n)
    return dev <= e




def box_method(f_goal, x0, dx, alfa, constraints, xd, xg, e, starting_point_modification_enabled=True):
    G = lambda x1, x2: sum([-(1 if constraint(x1, x2) < -EPS else 0) * constraint(x1, x2) for constraint in constraints])

    x0 = get_ok_point_for_explicit_constraints(x0, xd, xg)

    if starting_point_modification_enabled:
        if not inequality_constraints_ok(G, x0):
            _, x0 = hooke_jeeves_method(f_goal=G, dx=dx, x0=x0, e=[e for _ in range(len(x0))])

    x = generate_2n_points(x0, xd, xg, G)

    F = GoalFunction(f_goal, is_multi_input=True)
    num_iter = 0

    best_f_val = None
    best_x_val = None
    best_f_val_bef_iter = 0
    while True:
        num_iter += 1
        x_evaluated = [F.evaluate(xi) for xi in x]

        l_ind = get_index_l(x_evaluated)
        f_eval_x = x_evaluated[l_ind]


        if best_f_val_bef_iter == DIV_NUM_ITER:
            raise DivergenceException("Divergence warning!", best_x_val, best_f_val, num_iter, F.called_times)


        if best_f_val is None:
            best_f_val = f_eval_x
            best_x_val = x[l_ind]
            best_f_val_bef_iter += 1
        else:
            if abs(f_eval_x) >= DIV_MAX_NUM or abs(f_eval_x) <= DIV_MIN_NUM:
                raise DivergenceException("Divergence warning - too big/small x!", best_x_val, best_f_val, num_iter, F.called_times)
            if f_eval_x < best_f_val:  # better result
                best_f_val = f_eval_x
                best_x_val = x[l_ind]
                best_f_val_bef_iter = 0
            else:  # = ili >
                best_f_val_bef_iter += 1

        h_ind = get_index_h(x_evaluated)
        xh = x[h_ind][:]
        h2_ind = get_index_h2(x_evaluated, h_ind)
        xh2 = x[h2_ind][:]

        xc = get_centroid_wo_h(x, h_ind)

        xr = get_reflection_point(xh, xc, alfa)
        xr = get_ok_point_for_explicit_constraints(xr, xd, xg)
        if not inequality_constraints_ok(G, xr):
            _, xr = hooke_jeeves_method(f_goal=G, dx=dx, x0=xr, e=[e for _ in range(len(x0))])

        if F.evaluate(xr) > F.evaluate(xh2):
            xr = shift_point_towards_centroid(xr, xc)
        x[h_ind] = xr[:]

        f_xc = F.evaluate(xc)

        if simplex_stopping_criterion_ok(f_xc, x, e, F):
            break

    x_evaluated = [F.evaluate(xi) for xi in x]
    l_ind = get_index_l(x_evaluated)
    x_best = x[l_ind]

    return num_iter, F.called_times, x_best




def diff_below_eps(x, xs, e):
    n = len(x)
    for i in range(n):
        if abs(x[i] - xs[i]) > e[i]:
            return False
    return True


def transform_problem_wo_constraints_method(f_goal, x0, dx, t0, t_scale_factor, inequality_constraints, equality_constraints, e, starting_point_modification_enabled=True):
    G = lambda x1, x2: sum(
        [-(1 if constraint(x1, x2) < -EPS else 0) * constraint(x1, x2) for constraint in inequality_constraints])

    if starting_point_modification_enabled:
        print("START x0 (before) =", x0)
        if not inequality_constraints_ok(G, x0):
            print("correcting x0...")
            _, x0 = hooke_jeeves_method(f_goal=G, dx=dx, x0=x0, e=[e for _ in range(len(x0))])
        print("START x0 (after) =", x0)

    f_log = lambda x: log(x) if x > 0 else float("-inf")

    U = lambda x, t: f_goal(*x) - 1 / t * sum(
        [f_log(ineq_constraint(*x)) for ineq_constraint in inequality_constraints]) + t * sum(
        [eq_constraint(*x) ** 2 for eq_constraint in equality_constraints])

    F = SurrogateGoalFunction()

    x = x0
    t = t0

    num_iter = 0

    best_f_val = None
    best_x_val = None
    best_f_val_bef_iter = 0
    while True:
        num_iter += 1
        xs = x

        if best_f_val_bef_iter == DIV_NUM_ITER:
            raise DivergenceException("Divergence warning!", best_x_val, best_f_val, num_iter, F.called_times)

        f_goal_t = lambda x: U(x, t)
        F.f = f_goal_t

        f_eval_x = f_goal_t(x)
        F.called_times += 1
        if best_f_val is None:
            best_f_val = f_eval_x
            best_x_val = x
            best_f_val_bef_iter += 1
        else:
            if abs(f_eval_x) >= DIV_MAX_NUM or abs(f_eval_x) <= DIV_MIN_NUM:
                raise DivergenceException("Divergence warning - too big/small x!", best_x_val, best_f_val, num_iter,
                                          F.called_times)
            if f_eval_x < best_f_val:  # better result
                best_f_val = f_eval_x
                best_x_val = x
                best_f_val_bef_iter = 0
            else:  # = ili >
                best_f_val_bef_iter += 1

        _, x = hooke_jeeves_method(f_goal=F, dx=dx, x0=x, e=[e for _ in range(len(x0))],
                                   is_surrogate_goal_fun=True)

        if (diff_below_eps(x, xs, [e for _ in range(len(x0))])):
            break

        t *= t_scale_factor     # e.g. t_scale_factor = 10

    return num_iter, F.called_times, x


def grad_descent(params_file_name, f_goal, d_f_goal):
    with open(params_file_name, "r") as grad_descent_params_file:
        lines = grad_descent_params_file.readlines()
        lines = [l.split("=") for l in lines]
        for l in lines:
            if l[0].strip() == "e":
                e = float(l[1].strip())
            elif l[0].strip() == "h":
                h = float(l[1].strip())
            elif l[0].strip() == "x0":
                x0 = l[1].strip().split()
                x0 = [float(el) for el in x0]
            else: # golden_ratio_yn = 1 or 0
                golden_ratio_yn = int(l[1]) == 1

        num_iter, F_called_times, x = grad_descent_method(f_goal=f_goal, d_f_goal=d_f_goal, x0=x0, h=h, golden_ration_yn=golden_ratio_yn, e=e)

    return num_iter, F_called_times, x


def newton_raphson(params_file_name, f_goal, d_f_goal, H_f_goal):
    with open(params_file_name, "r") as newton_raphson_params_file:
        lines = newton_raphson_params_file.readlines()
        lines = [l.split("=") for l in lines]
        for l in lines:
            if l[0].strip() == "e":
                e = float(l[1].strip())
            elif l[0].strip() == "h":
                h = float(l[1].strip())
            elif l[0].strip() == "x0":
                x0 = l[1].strip().split()
                x0 = [float(el) for el in x0]
            else: # golden_ratio_yn = 1 or 0
                golden_ratio_yn = int(l[1]) == 1

        num_iter, F_called_times, x = newton_raphson_method(f_goal=f_goal, d_f_goal=d_f_goal, H_f_goal=H_f_goal, x0=x0, h=h, golden_ration_yn=golden_ratio_yn, e=e)

    return num_iter, F_called_times, x


def box(params_file_name, f_goal, constraints, xd, xg):

    with open(params_file_name, "r") as box_params_file:
        lines = box_params_file.readlines()
        lines = [l.split("=") for l in lines]
        for l in lines:
            if l[0].strip() == "e":
                e = float(l[1].strip())
            elif l[0].strip() == "alfa":
                alfa = float(l[1].strip())
            elif l[0].strip() == "x0":
                x0 = l[1].strip().split()
                x0 = [float(el) for el in x0]
            else: # dx
                dx = l[1].strip().split()
                dx = [float(el) for el in dx]

        num_iter, F_called_times, x = box_method(f_goal=f_goal, x0=x0, dx=dx, alfa=alfa, constraints=constraints, xd=xd, xg=xg, e=e)

    return num_iter, F_called_times, x


def transform_problem_wo_constraints(params_file_name, f_goal, inequality_constraints, equality_constraints, starting_point_modification_enabled=True):
    with open(params_file_name, "r") as box_params_file:
        lines = box_params_file.readlines()
        lines = [l.split("=") for l in lines]
        for l in lines:
            if l[0].strip() == "e":
                e = float(l[1].strip())
            elif l[0].strip() == "t0":
                t0 = float(l[1].strip())
            elif l[0].strip() == "t_scale_factor":
                t_scale_factor = float(l[1].strip())
            elif l[0].strip() == "x0":
                x0 = l[1].strip().split()
                x0 = [float(el) for el in x0]
            else: # dx
                dx = l[1].strip().split()
                dx = [float(el) for el in dx]

        num_iter, F_called_times, x = transform_problem_wo_constraints_method(f_goal=f_goal, x0=x0, dx=dx, t0=t0, t_scale_factor=t_scale_factor,
                                                inequality_constraints=inequality_constraints,
                                                equality_constraints=equality_constraints,
                                                e=e, starting_point_modification_enabled=starting_point_modification_enabled)

    return num_iter, F_called_times, x







def zad1():
    print("------------------------ ZAD 1 ------------------------")
    f3 = lambda x1, x2 : (x1-2)**2 + (x2+3)**2
    df3 = lambda x1, x2: [2*(x1-2), 2*(x2+3)]

    print("**********")
    print("GD + golden ratio:")
    try:
        num_iter, F_called_times, x = grad_descent("grad_desc_params_y_f3.txt", f3, df3)
        print("num_iter =",num_iter)
        print("F_called_times =",F_called_times)
        print("x = ", x, " ; f(x) =",f3(*x))
    except DivergenceException as err:
        print(err.msg)
        print("num_iter =", err.num_iter)
        print("F_called_times =", err.f_called_times)
        print("x = ", err.x, " ; f(x) =", err.f_x)

    print()
    print("**********")
    print("GD (without golden ratio):")
    try:
        num_iter, F_called_times, x = grad_descent("grad_desc_params_n_f3.txt", f3, df3)
        print("num_iter =",num_iter)
        print("F_called_times =",F_called_times)
        print("x = ", x, " ; f(x) =",f3(*x))
    except DivergenceException as err:
        print(err.msg)
        print("num_iter =", err.num_iter)
        print("F_called_times =", err.f_called_times)
        print("x = ", err.x, " ; f(x) =", err.f_x)

    ## True
    # GD
    # num_iter = 1
    # F_called_times = 36
    # x =  [1.9999997462314094, -2.999999619347114]
    # f(x) = 2.0929511726453027e-13

    ## False
    # GD
    # Divergence warning!
    # num_iter = 101
    # F_called_times = 100
    # x =  [0, 0]  ; f(x) = 13


def zad2():
    print()
    print("------------------------ ZAD 2 ------------------------")

    print("---------------- f1 ----------------")
    f1 = lambda x1, x2: 100 * (x2 - x1 ** 2) ** 2 + (1 - x1) ** 2
    df1 = lambda x1, x2:  [2 * (200*x1**3 - 200*x1*x2 + x1 - 1), 200 * (x2 - x1**2)]
    H1 = lambda x1, x2: [[1200*x1**2 - 400*x2 + 2, -400*x1], [-400*x1, 200]]

    print("**********")
    print("GD + golden ratio:")
    try:
        num_iter, F_called_times, x = grad_descent("grad_desc_params_y_f1.txt", f1, df1)
        print("num_iter =", num_iter)
        print("F_called_times =", F_called_times)
        print("grad(F) calculated times =", num_iter+1)
        print("hessian(F) calculated times =", 0)
        print("x = ", x, " ; f(x) =", f1(*x))
    except DivergenceException as err:
        print(err.msg)
        print("num_iter =", err.num_iter)
        print("F_called_times =", err.f_called_times)
        print("grad(F) calculated times =", err.num_iter + 1)
        print("hessian(F) calculated times =", 0)
        print("x = ", err.x, " ; f(x) =", err.f_x)

    print()
    print("**********")
    print("NR + golden ratio:")
    try:
        num_iter, F_called_times, x = newton_raphson("newton_params_y_f1.txt", f1, df1, H1)
        print("num_iter =", num_iter)
        print("F_called_times =", F_called_times)
        print("grad(F) calculated times =", num_iter+1)
        print("hessian(F) calculated times =", num_iter+1)
        print("x = ", x, " ; f(x) =", f1(*x))
    except DivergenceException as err:
        print(err.msg)
        print("num_iter =", err.num_iter)
        print("F_called_times =", err.f_called_times)
        print("grad(F) calculated times =", err.num_iter+1)
        print("hessian(F) calculated times =", err.num_iter+1)
        print("x = ", err.x, " ; f(x) =", err.f_x)

    ## True
    # GD
    # num_iter = 4080
    # F_called_times = 146880
    # x = [1.0000010334019551, 1.0000020708085102];
    # f(x) = 1.0695224278303312e-12

    # NR
    # num_iter = 15
    # F_called_times = 563
    # x =  [0.9999999999995737, 0.9999999999991585]
    # f(x) = 1.9407950420699943e-25


    print()
    print("---------------- f2 ----------------")

    f2 = lambda x1, x2: (x1 - 4) ** 2 + 4 * (x2 - 2) ** 2
    df2 = lambda x1, x2: [2 * x1 - 8, 8 * x2 - 16]
    H2 = lambda x1, x2: [[2, 0], [0, 8]]


    print("**********")
    print("GD + golden ratio:")
    try:
        num_iter,F_called_times, x = grad_descent("grad_desc_params_y_f2.txt", f2, df2)
        print("num_iter =", num_iter)
        print("F_called_times =",F_called_times)
        print("grad(F) calculated times =", num_iter+1)
        print("hessian(F) calculated times =", 0)
        print("x = ", x, " ; f(x) =", f2(*x))
    except DivergenceException as err:
        print(err.msg)
        print("num_iter =", err.num_iter)
        print("F_called_times =", err.f_called_times)
        print("grad(F) calculated times =", err.num_iter+1)
        print("hessian(F) calculated times =", 0)
        print("x = ", err.x, " ; f(x) =", err.f_x)

    print()
    print("**********")
    print("NR + golden ratio:")
    try:
        num_iter, F_called_times, x = newton_raphson("newton_params_y_f2.txt", f2, df2, H2)
        print("num_iter =", num_iter)
        print("F_called_times =",F_called_times)
        print("grad(F) calculated times =", num_iter+1)
        print("hessian(F) calculated times =", num_iter+1)
        print("x = ", x, " ; f(x) =", f2(*x))
    except DivergenceException as err:
        print(err.msg)
        print("num_iter =", err.num_iter)
        print("F_called_times =", err.f_called_times)
        print("grad(F) calculated times =", err.num_iter+1)
        print("hessian(F) calculated times =", err.num_iter+1)
        print("x = ", err.x, " ; f(x) =", err.f_x)

    print()
    print("**********")
    print("NR + golden ratio (eps = 1e-4):")
    try:
        # put greater tolerance (e=1e-4) to get num_iter = 1 (now num_iter = 2 due to floating point errors)
        num_iter, F_called_times, x = newton_raphson("newton_params_y_e4_f2.txt", f2, df2, H2)
        print("num_iter =", num_iter)
        print("F_called_times =",F_called_times)
        print("grad(F) calculated times =", num_iter+1)
        print("hessian(F) calculated times =", num_iter+1)
        print("x = ", x, " ; f(x) =", f2(*x))
    except DivergenceException as err:
        print(err.msg)
        print("num_iter =", err.num_iter)
        print("F_called_times =", err.f_called_times)
        print("grad(F) calculated times =", err.num_iter+1)
        print("hessian(F) calculated times =", err.num_iter+1)
        print("x = ", err.x, " ; f(x) =", err.f_x)

    ## True
    # GD
    # num_iter = 27
    # F_called_times = 972
    # x = [3.9999996489378384, 2.0000000503305166];
    # f(x) = 1.3337728490375497e-13

    # NR
    # num_iter = 2
    # F_called_times = 74
    # x = [4.000000000000164, 2.0000000000000715];
    # f(x) = 4.7446828005422976e-26

    # NR (eps = 1e-4)
    # num_iter = 1
    # F_called_times = 27
    # x = [4.0, 2.0];
    # f(x) = 0.0

def zad3():
    print()
    print("------------------------ ZAD 3 ------------------------")

    constraints_lst = [lambda x1, x2: x2 - x1, lambda x1, x2: 2 - x1]


    print("---------------- f1 ----------------")
    # min: (1,1) -> 0
    # local min: (0.0102081, 0.0102084) -> 0.989898
    f1 = lambda x1, x2: 100 * (x2 - x1 ** 2) ** 2 + (1 - x1) ** 2

    try:
        num_iter, F_called_times, x = box("box_f1.txt", f1, constraints=constraints_lst, xd = [-100, -100], xg = [100, 100])
        print("num_iter =",num_iter)
        print("F_called_times =",F_called_times)
        print("x = ", x, " ; f(x) =",f1(*x))
    except DivergenceException as err:
        print(err.msg)
        print("num_iter =", err.num_iter)
        print("F_called_times =", err.f_called_times)
        print("x = ", err.x, " ; f(x) =", err.f_x)

    # Divergence warning!
    # num_iter = 277
    # F_called_times = 3040
    # x = [1.0438695053353106, 1.0807635129804436];
    # f(x) = 0.009845589014067851

    # Divergence warning!
    # num_iter = 145
    # F_called_times = 1588
    # x = [-0.04759182040688148, -0.0138002091896689];
    # f(x) = 1.1232576569540975

    print()
    print("---------------- f2 ----------------")
    ## min: (2,2) -> 4
    f2 = lambda x1, x2: (x1 - 4) ** 2 + 4 * (x2 - 2) ** 2

    try:
        num_iter, F_called_times, x = box("box_f2.txt", f2, constraints=constraints_lst, xd = [-100, -100], xg = [100, 100])
        print("num_iter =", num_iter)
        print("F_called_times =", F_called_times)
        print("x = ", x, " ; f(x) =", f2(*x))
    except DivergenceException as err:
        print(err.msg)
        print("num_iter =", err.num_iter)
        print("F_called_times =", err.f_called_times)
        print("x = ", err.x, " ; f(x) =", err.f_x)

    # Divergence warning!
    # num_iter = 152
    # F_called_times = 1665
    # x = [1.9373618156929062, 2.0904933063565125];
    # f(x) = 4.287232433342999



def zad4():
    print()
    print("------------------------ ZAD 4 ------------------------")

    inequality_constraints_lst = [lambda x1, x2: x2 - x1, lambda x1, x2: 2 - x1]
    equality_constraints = []

    print("---------------- f1 ----------------")
    # min: (1,1) -> 0 - to je pravi min, ali ga ne nalazi uvijek (visemodalni problem) - rjesenje (lok. min) ovisi o pocetnoj tocki
    # local min: (0.0102081, 0.0102084) -> 0.989898
    f1 = lambda x1, x2: 100 * (x2 - x1 ** 2) ** 2 + (1 - x1) ** 2

    print("**** 'Bad' x0:")
    try:
        x0 = [-1.9, 2]
        print("x0 =",x0," ; f(x0) =",f1(*x0))
        num_iter, F_called_times, x = transform_problem_wo_constraints("transf_wo_constr_bad_f1.txt", f1, inequality_constraints=inequality_constraints_lst, equality_constraints=equality_constraints)
        print("num_iter =", num_iter)
        print("F_called_times =", F_called_times)
        print("x = ", x, " ; f(x) =", f1(*x))
    except DivergenceException as err:
        print(err.msg)
        print("num_iter =", err.num_iter)
        print("F_called_times =", err.f_called_times)
        print("x = ", err.x, " ; f(x) =", err.f_x)

    print()
    print("**** 'Good' x0:")
    try:
        x0 = [0, 2]
        print("x0 =", x0, " ; f(x0) =", f1(*x0))
        num_iter, F_called_times, x = transform_problem_wo_constraints("transf_wo_constr_good_f1.txt", f1,
                                                                       inequality_constraints=inequality_constraints_lst,
                                                                       equality_constraints=equality_constraints)
        print("num_iter =", num_iter)
        print("F_called_times =", F_called_times)
        print("x = ", x, " ; f(x) =", f1(*x))
    except DivergenceException as err:
        print(err.msg)
        print("num_iter =", err.num_iter)
        print("F_called_times =", err.f_called_times)
        print("x = ", err.x, " ; f(x) =", err.f_x)


    print()
    print("---------------- f2 ----------------")
    ## min: (2,2) -> 4
    f2 = lambda x1, x2: (x1 - 4) ** 2 + 4 * (x2 - 2) ** 2

    try:
        num_iter, F_called_times, x = transform_problem_wo_constraints("transf_wo_constr_f2.txt", f2,
                                                                       inequality_constraints=inequality_constraints_lst,
                                                                       equality_constraints=equality_constraints)
        print("num_iter =", num_iter)
        print("F_called_times =", F_called_times)
        print("x = ", x, " ; f(x) =", f2(*x))
    except DivergenceException as err:
        print(err.msg)
        print("num_iter =", err.num_iter)
        print("F_called_times =", err.f_called_times)
        print("x = ", err.x, " ; f(x) =", err.f_x)

def zad5():
    print()
    print("------------------------ ZAD 5 ------------------------")

    ## min: min: (2, 1) -> 2
    inequality_constraints_lst = [lambda x1, x2: 3 - x1 - x2, lambda x1, x2: 3 + 1.5 * x1 - x2]
    equality_constraints = [lambda x1, x2: x2 - 1]


    print("**** 'Bad' x0 (starting_point_modification_enabled = False):")
    try:
        f4 = lambda x1, x2: (x1 - 3) ** 2 + x2 ** 2
        # x0 = [5,5]

        num_iter, F_called_times, x = transform_problem_wo_constraints("transf_wo_constr_bad_f4.txt", f4,
                                                                       inequality_constraints=inequality_constraints_lst,
                                                                       equality_constraints=equality_constraints, starting_point_modification_enabled=False)

        print("num_iter =",num_iter)
        print("F_called_times =",F_called_times)
        print("x = ", x, " ; f(x) =",f4(*x))

    except DivergenceException as err:
        print(err.msg)
        print("num_iter =", err.num_iter)
        print("F_called_times =", err.f_called_times)
        print("x = ", err.x, " ; f(x) =", err.f_x)

    print()
    print("**** 'Bad' x0 (starting_point_modification_enabled = True):")
    try:
        f4 = lambda x1, x2: (x1 - 3) ** 2 + x2 ** 2
        # x0 = [5, 5]

        num_iter, F_called_times, x = transform_problem_wo_constraints("transf_wo_constr_bad_f4.txt", f4,
                                                                       inequality_constraints=inequality_constraints_lst,
                                                                       equality_constraints=equality_constraints,
                                                                       starting_point_modification_enabled=True)

        print("num_iter =", num_iter)
        print("F_called_times =", F_called_times)
        print("x = ", x, " ; f(x) =", f4(*x))

    except DivergenceException as err:
        print(err.msg)
        print("num_iter =", err.num_iter)
        print("F_called_times =", err.f_called_times)
        print("x = ", err.x, " ; f(x) =", err.f_x)


    print()
    print("**** 'Good' x0:")
    try:
        f4 = lambda x1, x2: (x1 - 3) ** 2 + x2 ** 2
        # x0 = [0, 0]

        num_iter, F_called_times, x = transform_problem_wo_constraints("transf_wo_constr_good_f4.txt", f4,
                                                                       inequality_constraints=inequality_constraints_lst,
                                                                       equality_constraints=equality_constraints)
        print("num_iter =", num_iter)
        print("F_called_times =", F_called_times)
        print("x = ", x, " ; f(x) =", f4(*x))

    except DivergenceException as err:
        print(err.msg)
        print("num_iter =", err.num_iter)
        print("F_called_times =", err.f_called_times)
        print("x = ", err.x, " ; f(x) =", err.f_x)

if __name__ == "__main__":
    zad1()
    zad2()
    zad3()
    zad4()
    zad5()