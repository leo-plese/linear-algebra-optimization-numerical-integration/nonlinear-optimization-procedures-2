Programsko okruženje: PyCharm 2019.2.4
Programski jezik: Python 3.8

Upute za pokretanje:
U programskom okruženju se pokrene glavni program (bez argumenata) smješten u datoteci "main.py" sa Shift+F10 ili na tipku "Run 'main'".
Pokretanjem se izvršavaju svi zadaci iz vježbe.
Rezultat svakog zadatka se ispisuje na ekran.

Implementacijska napomena:
Prema preporuci, u Newton-Raphsonovom postupku se za rješavanje linearnog sustava koristi modul za LUP dekompoziciju razvijen u okviru 1. laboratorijske vježbe smješten u datoteci "matrix.py".
