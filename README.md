Nonlinear Optimization Procedures.

Topics:

- Steepest descent procedure (gradient descent)

- Newton-Raphson procedure

- Box procedure for constraint problems

- Procedure of transformation into a problem without constraints in a mixed way


Implemented in Python.

My lab assignment in Computer Aided Analysis and Design, FER, Zagreb.

Task description in "TaskSpecification.pdf".

Created: 2020

